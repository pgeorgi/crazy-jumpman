.include "common.asm"

screenstart=$4000
levelstart=$6000
putchar=$f000
exit=$0

	*=$1000

	lda #<screenstart
	sta scrpos
	lda #>screenstart
	sta scrpos+1

	; clean first line
	ldy #40
-
	lda #' '
	sta (scrpos), y
	dey
	bne -

	lda #<levelstart
	sta leveldata
	lda #>levelstart
	sta leveldata+1

	jsr decompressLevel

	lda #<screenstart
	sta scrpos
	lda #>screenstart
	sta scrpos+1

	ldy #0
-
	lda (scrpos), y
	jsr putchar
	#incAddr scrpos
	lda scrpos+1
	cmp #(4 + >screenstart)
	bne -

	jmp exit

.include "compression.asm"
