.include "common.asm"

screenstart=$4000
levelstart=$6000
putchar=$f000
exit=$0

	*=$1000

	lda #<screenstart
	sta scrpos
	lda #>screenstart
	sta scrpos+1

	lda #<levelstart
	sta leveldata
	lda #>levelstart
	sta leveldata+1

	jsr compressLevel

	lda #<levelstart
	sta scrpos
	lda #>levelstart
	sta scrpos+1

	ldy #0
-
	lda (scrpos), y
	jsr putchar
	#incAddr scrpos
	lda scrpos+1
	cmp leveldata+1
	bne -
	lda scrpos
	cmp leveldata
	bne -

	jmp exit

.include "compression.asm"
