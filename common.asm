.cpu "6502"

.enc "graphics"
.cdef "  ", $20
.cdef "()", $28
.cdef "-/", $2d
.cdef "09", $30
.cdef ":@", $3a
.cdef "**", $ce
.cdef "AZ", $41
.cdef "az", $61

.edef "{clr}", $93
.edef "{home}", $13
.edef "{crsr down}", $11
.edef "{crsr right}", $1d
.edef "{wall}", $00
.edef "_", $1d ; really: cursor right
.edef "{empty}", ' '
.edef "{money}", $3a
.edef "{snake}", $3b
.edef "{ladderTop}", 'K'
.edef "{ladderBtm}", 'L'
.edef "{startTop}", 'H'
.edef "{startBtm}", 'I'
.edef "{black}", $90
.edef "{ltblue}", $9a
.edef "{gray3}", $9b
.edef "{yellow}", $9e
.edef "\n", $be
.edef "{eom}", $bf
