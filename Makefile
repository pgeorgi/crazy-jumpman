LIB6502=lib6502-1.3

LEVELS=$(subst .prg,.compress,$(wildcard levels/*/*.prg))

OUT=crazy-jumpman.basic.prg
OUT+=$(LEVELS)

all: $(OUT)

clean:
	rm -f $(OUT)

-include .*.dep

%.prg: %.asm
	64tass --cbm-prg -M .$*.dep "$<" -o "$@" -L "$*.listing"

crazy-jumpman.basic.prg: $(LEVELS)

$(LIB6502)/run6502:
	make -C $(LIB6502)

do_compress.bin: do_compress.asm compression.asm
	64tass -Wall -b -o $@ $<

levels/%.compress: levels/%.prg $(LIB6502)/run6502 do_compress.bin
	$(LIB6502)/run6502 -l 3FFE $< -l 1000 do_compress.bin -R 1000 -P F000 -X 0 > $@.tmp
	mv $@.tmp $@
