.include "common.asm"

hardMode = $02
moneyCount = $B4 ; filled in by decompressLevel
ladderLocation = $B5 ; filled in by decompressLevel
sysvar_driveNo = $BA
tmpVal = $FB ; used in (de)compressLevel
arg2Addr = $FC
argAddr = $FE
kbdRepeatFlag = $0286
krnScreenPage = $0288
varJoystickDirs = $0400
varMoveHandler = $0401
varMoveUnknown = $0402
varTmp = $0403
varUnknown5 = $0405
varUnknown6 = $0406
varLevel = $0407
varScore = $0408
varHighscore = $040B
varLives = $040E
varUnknownF = $040F
varBonus = $0410
varBombs = $0412
varUnknown13 = $0413
varPlayCustomLvl = $0415
varUnknown16 = $0416
prevInput = $0417
varUnknown18 = $0418
varFilename = $0500
mapEditor = $c000
mapEditColor = $c3ee
mapEditBombNo = $c3ef
mapCurrent = $c400
font = $e000
font_size = $1000
font_orig = $d000
sys_chkColorCode = $E8CB

      sei
      lda  #$31
      sta  $01

      lda  #<font_orig
      sta  $62
      sta  $64
      lda  #>font_orig
      sta  $63
      lda  #>font
      sta  $65
      ldx  #>font_size
      ldy  #<font_size
-
      lda  ($62),y
      sta  ($64),y
      iny
      bne  -
      inc  $63
      inc  $65
      dex
      bne  -
;done copying
      lda  #$37
      sta  $01
      cli
      lda  #$18
      sta  $D018                        ; VIC memory control register
      lda  #$94
      sta  $DD00                        ; Data port A #2: serial bus, RS-232, VIC memory

; clear level buffer
      lda  #>mapEditor
      sta  krnScreenPage
      lda  #'{clr}'
      jsr  $FFD2                        ; Routine: Send a char in the channel

      lda  #>mapCurrent
      sta  krnScreenPage

; copy symbols into font
      ldx  #$A0
-
      lda  font_symbols-1,x
      sta  $e200-1,x
      dex
      bne  -

initGame
      lda  #$00
      sta  hardMode
      sta  varHighscore
      sta  varHighscore+1
      sta  varHighscore+2
      jsr  load_charset
      jsr  W935B

resetGame
      lda  #$01
      sta  varLevel
      lda  #$00
      sta  $D418                        ; Select volume and filter mode
      jsr  menuStart
      lda  $DC0E                        ; Control register A of CIA #1
      and  #$FE
      sta  $DC0E                        ; Control register A of CIA #1
      lda  #$00
      sta  varScore
      sta  varScore+1
      sta  varScore+2
      lda  #$04
      sta  varLives
      lda  #$0F
      sta  $D418                        ; Select volume and filter mode
      sta  varUnknown16

nextLevel
      lda  #$00
      sta  $D404                        ; Voice 1: Control registers
      sta  $D40B                        ; Voice 2: Control registers
      sta  $D412                        ; Voice 3: Control registers
      sta  varMoveHandler
      sta  varMoveUnknown
      sta  varUnknown5
      sta  varUnknown6
      sta  varUnknownF
      sta  varBonus
      lda  #$10
      sta  varBonus+1
      jsr  chooseLevel
      jsr  W80E7
      jsr  joy_handleUp
W8167
      jsr  W8176
      jsr  W8B34
      jsr  W8A24
      jsr  W82E4
      jmp  W8167

W8176
      lda  arg2Addr
      sta  varUnknown18
      lda  arg2Addr+1
      sta  varUnknown18+1
      lda  varUnknownF
      beq  +
      jmp  W89A6
+
      lda  varMoveUnknown
      beq  joy_process
      jmp  W82F2

joy_process
      lda  #$80
      sta  $D404                        ; Voice 1: Control registers
      jsr  W85E5
      lda  #$E0
      sta  $DC02                        ; Data direction register port A #1
      ldx  $DC00                        ; Data port A #1: keyboard, joystick, paddle, optical pencil
      lda  #$FF
      sta  $DC02                        ; Data direction register port A #1
      txa
      and  #$0D                         ; only care about left, right, and up
      cmp  #$0D
      beq  joy_noMovement
      stx  varJoystickDirs
      txa
      and  #$10
      beq  joy_handleFire
      txa
      and  #$08
      beq  joy_handleRight
      txa
      and  #$04
      beq  joy_handleLeft
      txa
      and  #$01
      beq  joy_handleUp
joy_noMovement
      txa
      and  #$10
      beq  joy_handleFire
      jsr  W85E5
      rts

joy_handleUp
      ldy  #$28
      lda  #$48
      sta  (arg2Addr),y
      lda  #$49
      ldy  #$50
      sta  (arg2Addr),y
      jsr  W82CD
      rts

joy_handleRight
      jmp  W827C

joy_handleFire
      ldx  varJoystickDirs
      txa
      and  #$09
      beq  joy_fireUpRight
      txa
      and  #$05
      beq  joy_fireUpLeft
      txa
      and  #$08
      beq  joy_fireRight
      txa
      and  #$04
      beq  joy_fireLeft
      txa
      and  #$01
      beq  joy_fireUp
      rts

joy_fireUpRight
      lda  #$01
      sta  varMoveHandler
      lda  #$06
      sta  varMoveUnknown
      rts

joy_fireUpLeft
      lda  #$02
      sta  varMoveHandler
      lda  #$06
      sta  varMoveUnknown
      rts

joy_fireRight
      lda  #$03
      sta  varMoveHandler
      lda  #$03
      sta  varMoveUnknown
      rts

joy_fireLeft
      lda  #$04
      sta  varMoveHandler
      lda  #$03
      sta  varMoveUnknown
      rts

joy_fireUp
      lda  #$05
      sta  varMoveHandler
      lda  #$04
      sta  varMoveUnknown
      rts

joy_handleLeft
      ldy  #$27
      lda  (arg2Addr),y
      jsr  W8560
      ldy  #$4F
      lda  (arg2Addr),y
      jsr  W8530
      ldy  #$28
      lda  (arg2Addr),y
      tax
      cpx  #$40
      beq  W824E
      cpx  #$44
      bne  W8255
W824E
      nop
      jsr  joy_handleUp
      jsr  W82E4
W8255
      cpx  #$42
      beq  +
      ldx  #$3E
+
      inx
      inx
      inx
      inx
      jsr  blank2CharSymbol
      ldy  #$27
      txa
      sta  (arg2Addr),y
      inx
      txa
      ldy  #$4F
      sta  (arg2Addr),y
      ldx  arg2Addr
      bne  +
      dec  arg2Addr+1
+
      dec  arg2Addr
      jsr  W82CD
      jsr  W8CE9
      rts

W827C
      ldy  #$29
      lda  (arg2Addr),y
      jsr  W8560
      ldy  #$51
      lda  (arg2Addr),y
      jsr  W8530
      ldy  #$28
      lda  (arg2Addr),y
      tax
      cpx  #$42
      beq  +
      cpx  #$46
      bne  ++
+
      jsr  joy_handleUp
      jsr  W82E4
+
      cpx  #$40
      beq  +
      ldx  #$3C
+
      inx
      inx
      inx
      inx
      jsr  blank2CharSymbol
      ldy  #$29
      txa
      sta  (arg2Addr),y
      inx
      txa
      ldy  #$51
      sta  (arg2Addr),y
      inc  arg2Addr
      bne  +
      inc  arg2Addr+1
+
      jsr  W82CD
      jsr  W8CE9
      rts

; blank out the two characters below
; the screen address pointed to by
; arg2Addr
blank2CharSymbol
      lda  #$20
      ldy  #$28
      sta  (arg2Addr),y
      ldy  #$50
      sta  (arg2Addr),y
      rts

W82CD
      lda  arg2Addr
      sta  argAddr
      lda  arg2Addr+1
      and  #$03
      ora  #$D8
      sta  argAddr+1
      ldy  #$28
      lda  #$01
      sta  (argAddr),y
      ldy  #$50
      sta  (argAddr),y
      rts

W82E4
      ldy  #$11
      ldx  #$00
-
      jsr  W848D
      dex
      bne  -
      dey
      bne  -
      rts

W82F2
      jsr  W8D03
      lda  varMoveHandler
      asl
      tax
      lda  jumptable,x
      sta  varTmp
      inx
      lda  jumptable,x
      sta  varTmp+1
      jmp  (varTmp)

jumptable
      .word $0000, joyFireUpRight, joyFireUpLeft, joyFireRight
      .word joyFireLeft, joyFireUp
joyFireUpRight
      lda  varMoveUnknown
      dec  varMoveUnknown
      cmp  #$06
      beq  W8335
      cmp  #$05
      beq  W8335
      cmp  #$04
      beq  W8338
      cmp  #$03
      beq  W8338
      cmp  #$02
      beq  W8342
      cmp  #$01
      beq  W8342
      rts

W8335
      jsr  arg2Minus40
W8338
      jsr  arg2Plus1
      jsr  W83C0
      jsr  W83EF
      rts

W8342
      jsr  arg2Plus40
      jsr  arg2Plus1
      jsr  W83C0
      jsr  W83EF
      rts

joyFireUpLeft
      lda  varMoveUnknown
      dec  varMoveUnknown
      cmp  #$06
      beq  W836E
      cmp  #$05
      beq  W836E
      cmp  #$04
      beq  W8371
      cmp  #$03
      beq  W8371
      cmp  #$02
      beq  W837B
      cmp  #$01
      beq  W837B
      rts

W836E
      jsr  arg2Minus40
W8371
      jsr  arg2Minus1
      jsr  W83C0
      jsr  W83DC
      rts

W837B
      jsr  arg2Plus40
      jsr  arg2Minus1
      jsr  W83C0
      jsr  W83DC
      rts

arg2Minus40
      sec
      lda  arg2Addr
      sbc  #$28
      sta  arg2Addr
      lda  arg2Addr+1
      sbc  #$00
      sta  arg2Addr+1
      rts

arg2Minus1
      sec
      lda  arg2Addr
      sbc  #$01
      sta  arg2Addr
      lda  arg2Addr+1
      sbc  #$00
      sta  arg2Addr+1
      rts

arg2Plus1
      clc
      lda  arg2Addr
      adc  #$01
      sta  arg2Addr
      lda  arg2Addr+1
      adc  #$00
      sta  arg2Addr+1
      rts

arg2Plus40
      clc
      lda  arg2Addr
      adc  #$28
      sta  arg2Addr
      lda  arg2Addr+1
      adc  #$00
      sta  arg2Addr+1
      rts

W83C0
      jsr  W84E7
      lda  varUnknown6
      beq  W83D8
      pla
      pla
      lda  varUnknown18
      sta  arg2Addr
      lda  varUnknown18+1
      sta  arg2Addr+1
      jsr  joy_handleUp
      rts

W83D8
      jsr  W85D4
      rts

W83DC
      jsr  W8652
      ldy  #$28
      lda  #$42
      sta  (arg2Addr),y
      lda  #$43
      ldy  #$50
      sta  (arg2Addr),y
      jsr  W82CD
      rts

W83EF
      jsr  W8652
      ldy  #$28
      lda  #$40
      sta  (arg2Addr),y
      lda  #$41
      ldy  #$50
      sta  (arg2Addr),y
      jsr  W82CD
      rts

joyFireRight
      lda  varMoveUnknown
      dec  varMoveUnknown
      cmp  #$03
      beq  W8415
      cmp  #$02
      beq  W8418
      cmp  #$01
      beq  W8422
      rts

W8415
      jsr  arg2Minus40
W8418
      jsr  arg2Plus1
      jsr  W83C0
      jsr  W83EF
      rts

W8422
      jsr  arg2Plus40
      jsr  arg2Plus1
      jsr  W83C0
      jsr  W83EF
      rts

joyFireLeft
      lda  varMoveUnknown
      dec  varMoveUnknown
      cmp  #$03
      beq  W8442
      cmp  #$02
      beq  W8445
      cmp  #$01
      beq  W844F
      rts

W8442
      jsr  arg2Minus40
W8445
      jsr  arg2Minus1
      jsr  W83C0
      jsr  W83DC
      rts

W844F
      jsr  arg2Plus40
      jsr  arg2Minus1
      jsr  W83C0
      jsr  W83DC
      rts

joyFireUp
      lda  varMoveUnknown
      dec  varMoveUnknown
      cmp  #$04
      beq  W8473
      cmp  #$03
      beq  W8473
      cmp  #$02
      beq  W8480
      cmp  #$01
      beq  W8480
      rts

W8473
      jsr  arg2Minus40
      jsr  W83C0
      jsr  W8652
      jsr  joy_handleUp
      rts

W8480
      jsr  arg2Plus40
      jsr  W83C0
      jsr  W8652
      jsr  joy_handleUp
      rts

W848D
      sei
      lda  #$00
      sta  $DC00                        ; Data port A #1: keyboard, joystick, paddle, optical pencil
      lda  $DC01                        ; Data port B #1: keyboard, joystick, paddle
      cmp  prevInput
      beq  +
      sta  prevInput
      cmp  #$FF
      beq  +
      lda  #$FE
      sta  $DC00                        ; Data port A #1: keyboard, joystick, paddle, optical pencil
      lda  $DC01                        ; Data port B #1: keyboard, joystick, paddle
      cli
      cmp  #$F7
      beq  toggleSound
      cmp  #$EF
      beq  rebootGame
      cmp  #$DF
      beq  cycleBorder
      cmp  #$BF
      beq  cycleBackground
      cmp  #$FD
      beq  W84E1
+
      cli
      rts

toggleSound
      lda  varUnknown16
      eor  #$0F
      sta  $D418                        ; Select volume and filter mode
      sta  varUnknown16
      pla
      pla
      rts

; why "reboot"? Because this clears the stack
rebootGame
      ldx  #$FF
      txs
      jmp  resetGame

cycleBorder
      inc  $D020                        ; Border color
      pla
      pla
      rts

cycleBackground
      inc  $D021                        ; Background 0 color
      pla
      pla
      rts

W84E1
      jsr  W8E12
      pla
      pla
      rts

W84E7
      lda  #$00
      sta  varUnknown6
      ldy  #$28
      lda  (arg2Addr),y
      jsr  W8560
      ldy  #$50
      lda  (arg2Addr),y
      jsr  W8530
      rts

W84FB
      cmp  #$20
      beq  W8513
      cmp  #$3A
      bcc  W8514
      cmp  #$54
      bcs  W8514
      cmp  #$51
      beq  W8514
      cmp  #$4D
      beq  W8514
      cmp  #$3C
      beq  W8514
W8513
      rts

W8514
      lda  #$00
      sta  varMoveUnknown
      lda  #$01
      sta  varUnknown6
      sta  varUnknown5
      pla
      pla
      pla
      pla
      lda  varUnknown18
      sta  arg2Addr
      lda  varUnknown18+1
      sta  arg2Addr+1
      rts

W8530
      cmp  #'{snake}'
      bne  +
      jmp  W8595
+
      cmp  #'O'
      bne  +
      jmp  W8595
+
      cmp  #'P'
      bne  +
      jmp  W8595
+
      cmp  #'J'
      bne  +
      jmp  W8595
+
      cmp  #'K'
      bne  +
      jmp  levelDone
+
      cmp  #'{money}'
      beq  collectMoney
      rts

      lda  varTmp
      cmp  #$20
      bne  W84FB
      rts

W8560
      sta  varTmp
      jsr  W84FB
      tya
      clc
      adc  #$28
      tay
      lda  (arg2Addr),y
      jsr  W84FB
      lda  varTmp
      jmp  W8530

collectMoney
      lda  #$00
      sta  varUnknown5
      dec  moneyCount
      bne  +
      ; got the last money bag
      ; paint ladder (even if it already exists)
      ldy  #0
      lda  #'{ladderTop}'
      sta  (ladderLocation), y
      ldy  #40
      lda  #'{ladderBtm}'
      sta  (ladderLocation), y
+
      clc
      sed
      lda  varScore+1
      adc  #$02
      sta  varScore+1
      bcc  +
      lda  varScore+2
      adc  #$00
      sta  varScore+2
      clc
      lda  varLives
      adc  #$01
      sta  varLives
      jsr  printStatusLine
+
      cld
      jsr  printScore
      jsr  W8E53
      rts

W8595
      ldx  #$FF
      txs
      lda  #$01
      sta  varUnknownF
      jsr  W8652
      jmp  W8167

levelDone
      clc
      sed
      lda  varBonus
      adc  varScore
      sta  varScore
      lda  varBonus+1
      adc  varScore+1
      sta  varScore+1
      lda  #0
      bcc  +
      lda  varScore+2
      adc  #$00
      sta  varScore+2
      clc
      lda  #1
+
      ldx  moneyCount
      bne  +
      ; extra life for collecting all money bags
      adc  #1
+
      adc  varLives
      sta  varLives
      jsr  printStatusLine
      cld
      jsr  printScore
      jsr  W8DCE
      lda  varLevel
      beq  +
      inc  varLevel
      jmp  nextLevel
+
      jmp  resetGame

W85D4
      ldy  #$78
      lda  (arg2Addr),y
      beq  W85DF
      cmp  #$51
      beq  W85DF
      rts

W85DF
      lda  #$00
      sta  varMoveUnknown
      rts

W85E5
      ldy  #$78
      lda  (arg2Addr),y
      beq  W85F5
      jsr  W8530
      cmp  #$51
      beq  W85F5
      jmp  W85FB

W85F5
      lda  #$00
      sta  varUnknown5
      rts

W85FB
      jsr  W8D1C
      jsr  blank2CharSymbol
      jsr  arg2Plus40
      jsr  joy_handleUp
      inc  varUnknown5
      lda  varUnknown5
      cmp  #$04
      bne  +
      jmp  W8595
+
      pla
      pla
      rts

W8625
      lda  #$7F
      sta  $DC0D                        ; Interrupt control register CIA #1
      sta  $DD0D                        ; Interrupt control register CIA #2
      sta  $DC00                        ; Data port A #1: keyboard, joystick, paddle, optical pencil
      lda  #$08
      sta  $DC0E                        ; Control register A of CIA #1
      sta  $DD0E                        ; Control register A of CIA #2
      sta  $DC0F                        ; Control register B of CIA #1
      sta  $DD0F                        ; Control register B of CIA #2
      ldx  #$00
      stx  $DC03                        ; Data direction register port B #1
      stx  $DD03                        ; Data direction register port A #2
      dex
      stx  $DC02                        ; Data direction register port A #1
      lda  #$94
      sta  $DD00                        ; Data port A #2: serial bus, RS-232, VIC memory
      jmp  $FDD0

W8652
      lda  varUnknown18
      sta  argAddr
      lda  varUnknown18+1
      sta  argAddr+1
      ldy  #$28
      lda  #$20
      sta  (argAddr),y
      ldy  #$50
      sta  (argAddr),y
      rts

chooseLevel
      lda  #'{clr}'
      jsr  $FFD2                        ; Routine: Send a char in the channel
      lda  varPlayCustomLvl
      beq  playShippedLvl
      jmp  playCustomLvl

playShippedLvl
      lda  varLevel
      cmp  numLvls
      bcs  outOfLevels
      sec
      sbc #1
      clc
      rol a
      tax
      clc
      lda  levelOffsets,x
      adc  #<numLvls
      sta  compress.leveldata
      inx
      lda  levelOffsets,x
      adc  #>numLvls
      sta  compress.leveldata+1
.cerror mapCurrent % 256 != 0, "mapCurrent not aligned"
      lda  #0
      sta  compress.scrpos
      lda  #>mapCurrent
      sta  compress.scrpos+1
      jsr  compress.decompressLevel
      jsr  printStatusLine
      lda  hardMode
      beq  +
      ; remove ladder again
      lda  #' '
      ldy  #0
      sta  (ladderLocation), y
      ldy  #40
      sta  (ladderLocation), y
+
      lda  #>mapCurrent+$3ee
      sta  argAddr+1
      lda  #<mapCurrent+$3ee
      sta  argAddr
      ldy  #0
      lda  (argAddr), y
      sta  mapEditColor
      sta  $D020                        ; Border color
      sta  $D021                        ; Background 0 color
      iny
      lda  (argAddr), y
      sta  varBombs
      jmp  W8AB1

outOfLevels
      jmp  resetGame

printStatusLine
      ldy  #$00
      ldx  #$17
      clc
      jsr  $FFF0                        ; Routine: Read/Set (X,Y) cursor position
      ldx  #$28
drawRow23
      lda  #$D1
      jsr  $FFD2                        ; Routine: Send a char in the channel
      dex
      bne  drawRow23
      lda  #$13
      jsr  $FFD2                        ; Routine: Send a char in the channel
      ldx  #$18
W86B4
      lda  #$CD
      jsr  $FFD2                        ; Routine: Send a char in the channel
      lda  #$11
      jsr  $FFD2                        ; Routine: Send a char in the channel
      lda  #$9D
      jsr  $FFD2                        ; Routine: Send a char in the channel
      dex
      bne  W86B4
      lda  #<txtStatusBar
      sta  argAddr
      lda  #>txtStatusBar
      sta  argAddr+1
      jsr  printStrBfDel
      jsr  printScore
      jsr  printLives
      jsr  printBonus
      jsr  printLevelNo
      jsr  printHighScore
      rts

txtStatusBar
      .text "{home}{gray3}SC<______ MEN<__ LE<__ HI<______ BO<{eom}"

printStrBfDel
      ldy  #$FF
W8963
      iny
      lda  (argAddr),y
      cmp  #$BF
      beq  W8970
      jsr  $FFD2                        ; Routine: Send a char in the channel
      jmp  W8963

W8970
      rts

printScore
      clc
      ldx  #$00
      ldy  #$03
      jsr  $FFF0                        ; Routine: Read/Set (X,Y) cursor position
      lda  varScore+2
      jsr  printBCD
      lda  varScore+1
      jsr  printBCD
      lda  varScore
      jsr  printBCD
      rts

printBCD
      sta  varTmp
      lsr
      lsr
      lsr
      lsr
      jsr  printDigit
      lda  varTmp
      and  #$0F
      jsr  printDigit
      rts

printDigit
      clc
      adc  #$30
      jsr  $FFD2                        ; Routine: Send a char in the channel
      rts

W89A6
      jsr  W89B3
      jsr  blank2CharSymbol
      jsr  arg2Plus40
      jsr  joy_handleUp
      rts

W89B3
      ldy  #$78
      lda  (arg2Addr),y
      beq  W89BE
      cmp  #$51
      beq  W89BE
      rts

W89BE
      sed
      sec
      lda  varLives
      sbc  #$01
      sta  varLives
      beq  W89E2
      cld
      ldy  #$28
      lda  #$52
      sta  (arg2Addr),y
      ldy  #$50
      lda  #$53
      sta  (arg2Addr),y
      jsr  W82CD
      jsr  W8D39
      pla
      pla
      jmp  nextLevel

W89E2
      cld
      jsr  printLives
      ldy  #$28
      lda  #$52
      sta  (arg2Addr),y
      ldy  #$50
      lda  #$53
      sta  (arg2Addr),y
      jsr  W82CD
      jsr  W8D39
      clc
      ldy  #$0F
      ldx  #$0B
      jsr  $FFF0                        ; Routine: Read/Set (X,Y) cursor position
      lda  #<strGameOver
      sta  argAddr
      lda  #>strGameOver
      sta  argAddr+1
      jsr  printStrBfDel
      jsr  chkNewHighscore
      lda  #$00
      sta  $C6
      jsr  W8D70
      pla
      pla
      jmp  resetGame

strGameOver
      .text "GAME OVER{eom}"
W8A24
      sed
      sec
      lda  varBonus
      beq  bonusReduce
      sbc  #$01
      sta  varBonus
      lda  varBonus+1
bonusReduceHi
      sbc  #$00
      sta  varBonus+1
      cld
      jsr  printBonus
      rts

printBonus
      clc
      ldx  #$00
      ldy  #$24
      jsr  $FFF0                        ; Routine: Read/Set (X,Y) cursor position
      lda  varBonus+1
      jsr  printBCD
      lda  varBonus
      jsr  printBCD
      rts

bonusReduce
      sbc  #$01
      sta  varBonus
      lda  varBonus+1
      bne  bonusReduceHi
      cld
      jmp  W8595

printLives
      clc
      ldx  #$00
      ldy  #$0E
      jsr  $FFF0                        ; Routine: Read/Set (X,Y) cursor position
      lda  varLives
      jsr  printBCD
      rts

printLevelNo
      clc
      ldx  #$00
      ldy  #$14
      jsr  $FFF0                        ; Routine: Read/Set (X,Y) cursor position
      lda  varLevel
      jsr  printBCD
      rts

W8AB1
      lda  varBombs
      sta  varUnknown13
      lda  #$00
      sta  varBombs
-
      lda  #$00
      sta  varTmp
      ldy  #$05
-
      lsr  $D012                        ; Reading/Writing IRQ balance value
      rol  varTmp
      jsr  rasterUnknown
      jsr  rasterUnknown
      dey
      bne  -
      lda  #$00
      sta  varTmp+1
      jsr  rasterUnknown
      lsr  $D012                        ; Reading/Writing IRQ balance value
      rol  varTmp+1
      jsr  rasterUnknown
      lsr  $D012                        ; Reading/Writing IRQ balance value
      rol  varTmp+1
      jsr  rasterUnknown
      lsr  $D012                        ; Reading/Writing IRQ balance value
      rol  varTmp+1
      clc
      lda  varTmp
      adc  varTmp+1
      tay
      lda  $C451,y
      cmp  #$20
      bne  --
      lda  #$50
      sta  $C451,y
      jsr  rasterUnknown
      lda  $D012                        ; Reading/Writing IRQ balance value
      and  #$07
      lsr  $D012                        ; Reading/Writing IRQ balance value
      adc  #$00
      tax
      lda  W8B20,x
      sta  $D851,y                      ; Color RAM
      dec  varUnknown13
      bne  --
      rts

W8B20
      .byte $03, $04, $05, $0a, $0c
      .byte $0d, $0e, $0f, $08
rasterUnknown
      lda  $D012                        ; Reading/Writing IRQ balance value
      and  #$1F
      tax
      inx
rasterWait
      dex
      bne  rasterWait
      rts

W8B34
      lda  #$5C
      sta  argAddr
      lda  #$C7
      sta  argAddr+1
W8B3C
      ldy  #$50
      lda  (argAddr),y
      sta  varTmp
      cmp  #$0F
      bne  +
      rts
+
      cmp  #$4F
      beq  W8B86
      cmp  #$4A
      beq  W8B62
      cmp  #$50
      beq  W8B5F
W8B54
      lda  argAddr
      bne  +
      dec  argAddr+1
+
      dec  argAddr
      jmp  W8B3C

W8B5F
      jmp  W8C3F

W8B62
      jsr  W8BD4
      jsr  W8BA2
      ldy  #$4F
      lda  (argAddr),y
      jsr  W8BEB
      bcs  W8B73+1
      lda  #$4A
W8B73
      bit  $4FA9       ; W8B73+1 = lda #$4f
      sta  (argAddr),y
      jsr  W8C26
      lda  argAddr
      bne  +
      dec  argAddr+1
+
      dec  argAddr
      jmp  W8B54

W8B86
      jsr  W8BD4
      jsr  W8BA2
      ldy  #$51
      lda  (argAddr),y
      jsr  W8BEB
      bcs  W8B97+1
      lda  #$4F
W8B97
      bit  $4AA9       ; W8B97+1 = lda #$4a
      sta  (argAddr),y
      jsr  W8C26
      jmp  W8B54

W8BA2
      ldy  #$78
      lda  (argAddr),y
      cmp  #$51
      beq  W8BDB
      cmp  #$40
      bcc  +
      cmp  #$4A
      bcs  +
      jmp  W8595

+
      cmp  #$20
      beq  +
      rts

+
      ldy  #$A0
      lda  (argAddr),y
      cmp  #$20
      beq  W8BC5+1
      lda  varTmp
W8BC5
      bit  $50A9       ; W8BC5+1 = lda #$50
      ldy  #$78
      sta  (argAddr),y
      jsr  W8C26
      pla
      pla
      jmp  W8B54

W8BD4
      ldy  #$50
      lda  #$20
      sta  (argAddr),y
      rts

W8BDB
      pla
      pla
W8BDD
      jsr  W8BD4
      lda  #$01
      sta  varBombs
      jsr  W8AB1
      jmp  W8B54

W8BEB
      sty  varTmp+1
      cmp  #$20
      bne  W8BF4
      clc
      rts

W8BF4
      cmp  #$40
      bcc  W8BFF
      cmp  #$4A
      bcs  W8BFF
      jmp  W8595

W8BFF
      cmp  #$4A
      beq  W8C07
      cmp  #$4F
      bne  W8C0B
W8C07
      ldy  #$50
      sec
      rts

W8C0B
      lda  varTmp+1
      sec
      sbc  #$28
      tay
      lda  (argAddr),y
      cmp  #$20
      beq  W8C1C
      ldy  #$50
      sec
      rts

W8C1C
      lda  #$50
      sta  (argAddr),y
      jsr  W8C26
      pla
      pla
      rts

W8C26
      sty  varTmp+1
      lda  argAddr
      sta  $F3
      lda  argAddr+1
      and  #$03
      ora  #$D8
      sta  $F4
      ldy  #$50
      lda  ($F3),y
      ldy  varTmp+1
      sta  ($F3),y
      rts

W8C3F
      ldy  #$78
      lda  (argAddr),y
      cmp  #$51
      beq  W8BDD
      cmp  #$40
      bcc  +
      cmp  #$4A
      bcs  +
      jmp  W8595

+
      cmp  #$20
      beq  W8C6A
      jsr  rasterUnknown
      lsr  $D012                        ; Reading/Writing IRQ balance value
      bcc  W8C60+1
      lda  #$4A
W8C60
      bit  $4FA9       ; W8C60+1 = lda #$4f
      ldy  #$50
      sta  (argAddr),y
      jmp  W8B54

W8C6A
      jsr  W8BD4
      ldy  #$78
      lda  #$50
      sta  (argAddr),y
      jsr  W8C26
      jmp  W8B54

chkNewHighscore
      sed
      lda  varHighscore+2
      cmp  varScore+2
      bcc  updateHighscore
      bne  chkNoNewHiscore
      lda  varHighscore+1
      cmp  varScore+1
      bcc  updateHighscore
      bne  chkNoNewHiscore
      lda  varHighscore
      cmp  varScore
      bcc  updateHighscore
chkNoNewHiscore
      cld
      rts

updateHighscore
      cld
      lda  varScore
      sta  varHighscore
      lda  varScore+1
      sta  varHighscore+1
      lda  varScore+2
      sta  varHighscore+2
      jsr  printHighScore
      ldx  #$0D
      ldy  #$0F
      clc
      jsr  $FFF0                        ; Routine: Read/Set (X,Y) cursor position
      lda  #<strNewHiscore
      sta  argAddr
      lda  #>strNewHiscore
      sta  argAddr+1
      jsr  printStrBfDel
      rts

strNewHiscore
      .text "NEW HISCORE{eom}"
printHighScore
      ldx  #$00
      ldy  #$1A
      clc
      jsr  $FFF0                        ; Routine: Read/Set (X,Y) cursor position
      lda  varHighscore+2
      jsr  printBCD
      lda  varHighscore+1
      jsr  printBCD
      lda  varHighscore
      jsr  printBCD
      rts

W8CE9
      lda  #$20
      sta  $D405                        ; Generator 1: Attack/Decay
      lda  #$F3
      sta  $D406                        ; Generator 1: Sustain/Release
      lda  #$14
      sta  $D401                        ; Voice 1: Frequency control (hi byte)
      lda  #$81
      sta  $D404                        ; Voice 1: Control registers
      lda  #$80
      sta  $D404                        ; Voice 1: Control registers
      rts

W8D03
      lda  varMoveUnknown
      asl
      asl
      asl
      sta  $D401                        ; Voice 1: Frequency control (hi byte)
      lda  #$F0
      sta  $D406                        ; Generator 1: Sustain/Release
      lda  #$00
      sta  $D405                        ; Generator 1: Attack/Decay
      lda  #$11
      sta  $D404                        ; Voice 1: Control registers
      rts

W8D1C
      ldx  varUnknown5
      inx
      inx
      inx
      txa
      asl
      asl
      asl
      sta  $D401                        ; Voice 1: Frequency control (hi byte)
      lda  #$F0
      sta  $D406                        ; Generator 1: Sustain/Release
      lda  #$00
      sta  $D405                        ; Generator 1: Attack/Decay
      lda  #$11
      sta  $D404                        ; Voice 1: Control registers
      rts

W8D39
      lda  #$21
      sta  $D405                        ; Generator 1: Attack/Decay
      lda  #$F1
      sta  $D406                        ; Generator 1: Sustain/Release
      lda  #$14
      sta  varTmp
W8D48
      lda  varTmp
      sta  $D401                        ; Voice 1: Frequency control (hi byte)
      lda  #$20
      sta  $D404                        ; Voice 1: Control registers
      ldx  #$32
W8D55
      dex
      bne  W8D55
      lda  #$21
      sta  $D404                        ; Voice 1: Control registers
      ldy  #$32
      ldx  #$00
W8D61
      dex
      bne  W8D61
      dey
      bne  W8D61
      dec  varTmp
      bne  W8D48
      jsr  blank2CharSymbol
      rts

W8D70
      lda  #$00
      sta  varTmp
      sta  varTmp+1
      lda  #$0A
      sta  varUnknown13
      sta  $D405                        ; Generator 1: Attack/Decay
      sta  $D40C                        ; Generator 2: Attack/Decay
      sta  $D413                        ; Generator 3: Attack/Decay
      lda  #$F0
      sta  $D406                        ; Generator 1: Sustain/Release
      sta  $D40D                        ; Generator 2: Sustain/Release
      sta  $D414                        ; Generator 3: Sustain/Release
      lda  #$11
      sta  $D404                        ; Voice 1: Control registers
      lda  #$21
      sta  $D40B                        ; Voice 2: Control registers
      sta  $D412                        ; Voice 3: Control registers
W8D9E
      lda  varTmp
      sta  $D401                        ; Voice 1: Frequency control (hi byte)
      lda  varTmp+1
      sta  $D408                        ; Voice 2: Frequency control (hi byte)
      lda  varUnknown13
      sta  $D40F                        ; Voice 3: Frequency control (hi byte)
      inc  varTmp
      bne  W8D9E
      jsr  W8E39
      dec  varTmp+1
      bne  W8D9E
      dec  varUnknown13
      bne  W8D9E
W8DC2
      lda  #$00
      sta  $D412                        ; Voice 3: Control registers
      sta  $D404                        ; Voice 1: Control registers
      sta  $D40B                        ; Voice 2: Control registers
      rts

W8DCE
      lda  #$F0
      sta  $D406                        ; Generator 1: Sustain/Release
      sta  $D40D                        ; Generator 2: Sustain/Release
      lda  #$00
      sta  $D405                        ; Generator 1: Attack/Decay
      sta  $D40C                        ; Generator 2: Attack/Decay
      sta  varTmp
      sta  varTmp+1
      lda  #$81
      sta  $D404                        ; Voice 1: Control registers
      lda  #$21
      sta  $D40B                        ; Voice 2: Control registers
W8DEE
      lda  varTmp
      sta  $D401                        ; Voice 1: Frequency control (hi byte)
      lda  varTmp+1
      sta  $D408                        ; Voice 2: Frequency control (hi byte)
      ldx  #$14
W8DFC
      dex
      bne  W8DFC
      dec  varTmp
      bne  W8DEE
      inc  varTmp+1
      bne  W8DEE
      lda  #$00
      sta  $D404                        ; Voice 1: Control registers
      sta  $D40B                        ; Voice 2: Control registers
      rts

W8E12
      sei
W8E13
      jsr  W8E26
      bne  W8E24
      lda  #$00
      sta  $DC00                        ; Data port A #1: keyboard, joystick, paddle, optical pencil
      lda  $DC01                        ; Data port B #1: keyboard, joystick, paddle
      cmp  #$FF
      beq  W8E13
W8E24
      cli
      rts

W8E26
      lda  #$E0
      sta  $DC02                        ; Data direction register port A #1
      ldx  $DC00                        ; Data port A #1: keyboard, joystick, paddle, optical pencil
      lda  #$FF
      sta  $DC02                        ; Data direction register port A #1
      txa
      and  #$1F
      cmp  #$1F
      rts

W8E39
      sei
      jsr  W8E26
      bne  W8E4D
      lda  #$00
      sta  $DC00                        ; Data port A #1: keyboard, joystick, paddle, optical pencil
      lda  $DC01                        ; Data port B #1: keyboard, joystick, paddle
      cmp  #$FF
      bne  W8E4D
      cli
      rts

W8E4D
      cli
      pla
      pla
      jmp  W8DC2

W8E53
      lda  #$32
      sta  $D401                        ; Voice 1: Frequency control (hi byte)
      lda  #$96
      sta  $D402                        ; Voice 1: Wave form pulsation amplitude (lo byte)
      lda  #$F0
      sta  $D406                        ; Generator 1: Sustain/Release
      sta  $D40D                        ; Generator 2: Sustain/Release
      lda  #$00
      sta  $D405                        ; Generator 1: Attack/Decay
      sta  $D40C                        ; Generator 2: Attack/Decay
      lda  #$41
      sta  $D404                        ; Voice 1: Control registers
      sta  $D40B                        ; Voice 2: Control registers
      lda  #$0F
      sta  varTmp
      sta  varTmp+1
W8E7D
      lda  varTmp
      sta  $D403                        ; Voice 1: Wave form pulsation amplitude (hi byte)
      lda  varTmp+1
      sta  $D40A                        ; Voice 2: Wave form pulsation amplitude (hi byte)
      ldx  #$00
W8E8B
      dex
      bne  W8E8B
      dec  varTmp
      bne  W8E7D
      lda  #$0F
      sta  varTmp
      dec  varTmp+1
      bne  W8E7D
      lda  #$00
      sta  $D404                        ; Voice 1: Control registers
      sta  $D40B                        ; Voice 2: Control registers
      rts

load_charset
      ldx  #$68
-
      lda  font_numbers-1,x
      sta  font+($30*8)-1,x
      dex
      bne  -
      ldx  #$d8
-
      lda  font_letters-1,x
      sta  font-1,x
      dex
      bne  -
      rts

font_letters ; and @ which is the ground tile
      .binary "font.prg", 2+$000, 27*8
font_numbers ; 0-9, dollar, snake, colon
      .binary "font.prg", 2+$180, 13*8
font_symbols ; player, ladder, gravestone, bomb
      .binary "font.prg", 2+$200, 20*8

;Prints a string using KERNAL routines
;until it encounters $bf.
;$be is interpreted as $0d but there
;is nothing that indicates why.
printMnu
      ldy  #$00
      lda  (argAddr),y
      cmp  #$BF
      beq  printMnuDone
      cmp  #$BE
      bne  printMnuNot0d
      lda  #$0D
printMnuNot0d
      jsr  $FFD2                        ; Routine: Send a char in the channel
      inc  argAddr
      bne  printMnuLoopEnd
      inc  argAddr+1
printMnuLoopEnd
      jmp  printMnu

printMnuDone
      rts

W935B
      lda  #$06
      sta  $D020                        ; Border color
      sta  $D021                        ; Background 0 color
      lda  #<mnuMainScr
      sta  argAddr
      lda  #>mnuMainScr
      sta  argAddr+1
      jsr  printMnu
      lda  #$0F
      sta  $D418                        ; Select volume and filter mode
      lda  #$00
      sta  $D405                        ; Generator 1: Attack/Decay
      sta  $D40C                        ; Generator 2: Attack/Decay
      sta  $D413                        ; Generator 3: Attack/Decay
      lda  #$F0
      sta  $D406                        ; Generator 1: Sustain/Release
      sta  $D40D                        ; Generator 2: Sustain/Release
      sta  $D414                        ; Generator 3: Sustain/Release
      lda  #$21
      sta  $D404                        ; Voice 1: Control registers
      lda  #$11
      sta  $D40B                        ; Voice 2: Control registers
      sta  $D412                        ; Voice 3: Control registers
      lda  #$14
      sta  $D408                        ; Voice 2: Frequency control (hi byte)
W939B
      lda  #$96
      sta  varTmp+1
W93A0
      lda  #$08
      sta  varTmp
W93A5
      jsr  W93C1
W93A8
      ldy  varTmp
      sta  $E26F,y
      dec  varTmp
      bne  W93A5
      lda  varTmp+1
      sta  $D40F                        ; Voice 3: Frequency control (hi byte)
      dec  varTmp+1
      bne  W93A0
      jmp  W939B

W93C1
      jsr  $FFE4                        ; Routine: Take the char from keyboard buffer
      bne  W93E0
      jsr  W8E26
      bne  W93E0
      lda  varTmp
      sta  $D401                        ; Voice 1: Frequency control (hi byte)
      ldx  #$00
W93D3
      dex
      bne  W93D3
      ldx  #$08
W93D8
      lsr  $D012                        ; Reading/Writing IRQ balance value
      rol
      dex
      bne  W93D8
      rts

W93E0
      pla
      pla
      lda  #$00
      sta  $D404                        ; Voice 1: Control registers
      sta  $D40B                        ; Voice 2: Control registers
      sta  $D412                        ; Voice 3: Control registers
      rts

mnuHelp
      .text "{clr}\n\n"
      .text "m      @@@@@@ CRAZY JUMPMAN @@@@@@\n"
      .text "m\n"
      .text "m         ERKLIMME DAS GERUEST\n"
      .text "m       SAMMLE DIE GELDSAECKE EIN\n"
      .text "m        UND ERREICHE DIE LEITER\n"
      .text "m        BEVOR DIE ZEIT ABLAEUFT\n"
      .text "m     VERMEIDE BOMBEN UND SCHLANGEN\n"
      .text "m\n"
      .text "m         F1 ... NEUSTART\n"
      .text "m         F3 ... RAHMENFARBE\n"
      .text "m         F5 ... HINTERGRUNDFARBE\n"
      .text "m         F7 ... TON EIN/AUS\n"
      .text "m\n"
      .text "m         @@@@@  EDITOR  @@@@@\n"
      .text "m\n"
      .text "m     : ... GELDSACK  (DOPPELPUNKT)\n"
      .text "m     ; ... SCHLANGE  (STRICHPUNKT)\n"
      .text "m     k ... LEITER OBEN   (SHIFT K)\n"
      .text "m     l ... LEITER UNTEN  (SHIFT L)\n"
      .text "m     @ ... GERUEST h (KLAMMERAFFE)\n"
      .text "m                   i\n"
      .text "mqqqqqqqq ZURUECK MIT LEERTASTE qqqqqqq{eom}"
mnuMainScr
      .text "{clr}{yellow}"
      .text "    *********         ***        ***\n"
      .text "    **********        ****      ****\n"
      .text "    **      ***       ** **    ** **\n"
      .text "    **       **       **  **  **  **\n"
      .text "    **       **       **   ****   **\n"
      .text "    **       **       **    **    **\n"
      .text "    **      ***       **    **    **\n"
      .text "    **********        **          **\n"
      .text "    *********         **          **\n"
      .text "    **                **          **\n"
      .text "    **                **          **\n"
      .text "    **                **          **\n"
      .text "    **                **          **\n"
      .text "    **                **          **\n"
      .text "    **                **          **\n"
      .text "    **                **          **\n"
      .text "\n"
      .text "\n"
      .text "\n"
      .text "         VIDEO GAMES PRESENTS <\n"
      .text "\n"
      .text "              CRAZY JUMPMAN\n"
      .text "\n"
      .text "   COPYRIGHT 3/4.1984 BY PETER MENKE{eom}"
mnuEdit
      .text "SPIELPLAN\n"
      .text "\n"
      .text "______(A)BSPEICHERN\n"
      .text "______(L)ADEN\n"
      .text "______(E)NTWERFEN\n"
      .text "______(S)PIELEN\n"
      .text "______(Z)URUECK{eom}"
mnuMainMenu
      .text "_____(S)PIEL ODER\n"
      .text "_________(E)DITOR?\n"
      .text "\n"
      .text "_________(A)NLEITUNG{eom}"

difficultyAddr
      .word mnuMainEasy, mnuMainHard

mnuMainEasy
      .text "SPIEL(M)ODUS\n        > EINFACH\n          HERAUSFORDERND{eom}"
mnuMainHard
      .text "SPIEL(M)ODUS\n          EINFACH\n        > HERAUSFORDERND{eom}"

menuStart
      lda  #'{clr}'
      jsr  $FFD2                        ; Routine: Send a char in the channel
      ldx  #$0C
      ldy  #$04
      clc
      jsr  $FFF0                        ; Routine: Read/Set (X,Y) cursor position
      lda  #<mnuMainMenu
      sta  argAddr
      lda  #>mnuMainMenu
      sta  argAddr+1
      jsr  printMnu
      jsr  printDifficulty
      jsr  W8625
      cli
menuWait
      jsr  $FFE4
      cmp  #'S'
      beq  gameStart
      cmp  #'E'
      beq  editStart
      cmp  #'A'
      beq  helpStart
      cmp  #'M'
      beq  toggleDifficulty
      jmp  menuWait

helpStart
      lda  #<mnuHelp
      sta  argAddr
      lda  #>mnuHelp
      sta  argAddr+1
      jsr  printMnu
-
      jsr  $FFE4                        ; Routine: Take the char from keyboard buffer
      cmp  #' '
      bne  -
      jmp  menuStart

gameStart
      lda  #$00
      sta  varPlayCustomLvl
      rts

toggleDifficulty
      lda  #1
      eor  hardMode
      sta  hardMode
      jsr  printDifficulty
      jmp  menuWait

printDifficulty
      ldx  #22
      ldy  #9
      clc
      jsr  $FFF0
      lda  hardMode
      rol
      tay
      lda  difficultyAddr, y
      sta  argAddr
      lda  difficultyAddr+1, y
      sta  argAddr+1
      jsr  printMnu
      rts

editStart
      lda  #'{clr}'
      jsr  $FFD2                        ; Routine: Send a char in the channel
      ldx  #$09
      ldy  #$06
      clc
      jsr  $FFF0                        ; Routine: Read/Set (X,Y) cursor position
      lda  #<mnuEdit
      sta  argAddr
      lda  #>mnuEdit
      sta  argAddr+1
      jsr  printMnu

; wait for A, L, E, S or Z
-
      jsr  $FFE4                        ; Routine: Take the char from keyboard buffer
      cmp  #'A'
      beq  editSave
      cmp  #'L'
      beq  editLoad
      cmp  #'E'
      beq  editDesign
      cmp  #'S'
      beq  editPlay
      cmp  #'Z'
      bne  -
      jmp  menuStart

editSave
      jsr  mnuQFileInfo
      lda  #<mapEditor
      sta  argAddr
      lda  #>mapEditor
      sta  argAddr+1
      lda  #argAddr
      ldx  #<mapEditor + $400
      ldy  #>mapEditor + $400
      jsr  $FFD8                        ; Routine: Save the Ram to a device
      jmp  editStart

editLoad
      jsr  mnuQFileInfo
      lda  #$00
      jsr  $FFD5                        ; Routine: Load the Ram from a device
      jmp  editStart

editPlay
      lda  #$01
      sta  varPlayCustomLvl
      lda  #$00
      sta  varLevel
      rts

; copy 256b-aligned 1K from *argAddr to *arg2Addr
copy1K
      ldx  #$04
-
      lda  (argAddr),y
      sta  (arg2Addr),y
      dey
      bne  -
      inc  arg2Addr+1
      inc  argAddr+1
      dex
      bne  -
      rts

editDesign

; start by copying the buffer to the screen
      ldy  #<mapCurrent
      lda  #>mapCurrent
      sta  arg2Addr+1
      sty  arg2Addr
      lda  #>mapEditor
      sta  argAddr+1
      sty  argAddr
      jsr copy1K

      lda  mapEditColor
      sta  $D020                        ; Border color
      sta  $D021                        ; Background 0 color
      lda  #$00
      sta  varScore
      sta  varScore+1
      sta  varScore+2
      sta  varBonus
      sta  varBonus+1
      sta  varLives
      jsr  printStatusLine
      jsr  W80E7
      jsr  joy_handleUp
      ldx  #$0C
      ldy  #$14
      clc
      jsr  $FFF0                        ; Routine: Read/Set (X,Y) cursor position

      ; enable key repeat for all keys
      lda  #$80
      sta  $028A

      jsr  $FFCF                        ; Routine: Acept a char in the channel

      ; set normal key repeat (cursor, space, inst/del)
      lda  #$00
      sta  $028A

      ldy  #<mapCurrent
      lda  #>mapCurrent
      sta  argAddr+1
      sty  argAddr
      lda  #>mapEditor
      sta  arg2Addr+1
      sty  arg2Addr
      jsr copy1K

      lda  #'{clr}'
      jsr  $FFD2                        ; Routine: Send a char in the channel
      lda  #<mnuScrColors
      sta  argAddr
      lda  #>mnuScrColors
      sta  argAddr+1
      ldx  #$0C
      ldy  #$06
      clc
      jsr  $FFF0                        ; Routine: Read/Set (X,Y) cursor position
      jsr  printMnu
      lda  #$FF
      sta  kbdRepeatFlag
_waitForColor
      jsr  $FFE4                        ; Routine: Take the char from keyboard buffer
      beq  _waitForColor
      jsr  sys_chkColorCode
      lda  kbdRepeatFlag
      cmp  #$FF
      beq  _waitForColor
      sta  mapEditColor
      lda  #'{clr}'
      jsr  $FFD2                        ; Routine: Send a char in the channel
      lda  #<mnuBombCount
      sta  argAddr
      lda  #>mnuBombCount
      sta  argAddr+1
      ldx  #$0C
      ldy  #$06
      clc
      jsr  $FFF0                        ; Routine: Read/Set (X,Y) cursor position
      jsr  printMnu
_waitForDigit
      jsr  $FFE4                        ; Routine: Take the char from keyboard buffer
      cmp  #'0'
      bcc  _waitForDigit
      cmp  #'3'+1
      bcs  _waitForDigit
      sta  varTmp
      jsr  $FFD2                        ; Routine: Send a char in the channel
      lda  varTmp
      and  #$0F
      sta  varTmp
_waitForDigit_2
      jsr  $FFE4                        ; Routine: Take the char from keyboard buffer
      cmp  #'0'
      bcc  _waitForDigit_2
      cmp  #'9'+1
      bcs  _waitForDigit_2
      sta  varTmp+1
      jsr  $FFD2                        ; Routine: Send a char in the channel
      lda  varTmp+1
      and  #$0F
      sta  varTmp+1
      lda  varTmp
      asl
      asl
      asl
      clc
      adc  varTmp
      clc
      adc  varTmp
      clc
      adc  varTmp+1
      beq  _waitForDigit_2
      sta  mapEditBombNo
      jmp  editStart

mnuQFileInfo
      lda  #<mnuDevNo
      sta  argAddr
      lda  #>mnuDevNo
      sta  argAddr+1
      lda  #'{clr}'
      jsr  $FFD2                        ; Routine: Send a char in the channel
      ldx  #$0C
      ldy  #$06
      clc
      jsr  $FFF0                        ; Routine: Read/Set (X,Y) cursor position
      jsr  printMnu
readDriveNo
      jsr  $FFE4                        ; Routine: Take the char from keyboard buffer
      cmp  #'1'
      bcc  readDriveNo
      cmp  #'9'+1
      bcs  readDriveNo
      and  #$0F
      sta  sysvar_driveNo
      lda  #$01
      sta  $B9
      lda  #'{clr}'
      jsr  $FFD2                        ; Routine: Send a char in the channel
      lda  #<mnuFilename
      sta  argAddr
      lda  #>mnuFilename
      sta  argAddr+1
      ldx  #$0C
      ldy  #$06
      clc
      jsr  $FFF0                        ; Routine: Read/Set (X,Y) cursor position
      jsr  printMnu
      ldx  #$00
readFilename
      jsr  $FFCF                        ; Routine: Acept a char in the channel
      sta  varFilename,x
      inx
      cmp  #$0D
      bne  readFilename
      jsr  $FFD2                        ; Routine: Send a char in the channel
      dex
      txa
      ldx  #<varFilename
      ldy  #>varFilename
      jsr  $FFBD                        ; Routine: Set file name
      rts

mnuDevNo
      .text "GERAETEADRESSE (1-9) ?{eom}"
mnuFilename
      .text "FILENAME ? {eom}"
mnuScrColors
      .text "BALKENFARBE (FARBTASTE DRUECKEN)?{eom}"
mnuBombCount
      .text "{gray3}ANZAHL AN BOMBEN (01-39) ? {eom}"
playCustomLvl
      ldy  #<mapEditor
      lda  #>mapEditor
      sta  argAddr+1
      sty  argAddr
      lda  #>mapCurrent
      sta  arg2Addr+1
      sty  arg2Addr
      jsr  copy1K

      lda  #>mapCurrent
      sta  argAddr+1
      ldy  #$00
      ldx  #$04
W9997
      lda  (argAddr),y
      beq  W99DD
      cmp  #$4D
      beq  W99DD
      cmp  #$51
      beq  W99DD
      cmp  #$3A
      beq  W99D8
      cmp  #$3B
      beq  W99D3
      cmp  #$4B
      beq  W99CE
      cmp  #$4C
      beq  W99E0
W99B3
      dey
      bne  W9997
      inc  argAddr+1
      dex
      bne  W9997
      lda  mapEditColor
      sta  kbdRepeatFlag
      jsr  printStatusLine
      lda  mapEditBombNo
      sta  varBombs
      jsr  W8AB1
      rts

W99CE
      lda  #$03
      jmp  W99E0

W99D3
      lda  #$00
      jmp  W99E0

W99D8
      lda  #$07
      jmp  W99E0

W99DD
      lda  mapEditColor
W99E0
      sta  varTmp
      lda  argAddr+1
      and  #$03
      ora  #$D8
      sta  arg2Addr+1
      lda  argAddr
      sta  arg2Addr
      lda  varTmp
      sta  (arg2Addr),y
      jmp  W99B3

W80E7
      lda  #$34
      sta  arg2Addr
      sta  varUnknown18
      lda  #$C7
      sta  arg2Addr+1
      sta  varUnknown18+1
      rts

compress .binclude "compression.asm"

numLvls
      .byte (levelOffsetsEnd - levelOffsets) / 2

levelOffsets
      .word level1 - numLvls
      .word level2 - numLvls
      .word levelck1 - numLvls
      .word levelck2 - numLvls
      .word levelck3 - numLvls
      .word levelck4 - numLvls
      .word levelck5 - numLvls
      .word levelck6 - numLvls
      .word levelck7 - numLvls
      .word levelck8 - numLvls
      .word levelck9 - numLvls
      .word levelck10 - numLvls
      .word levelck11 - numLvls

levelOffsetsEnd

level1
      .binary "levels/original/lvl1.compress"
level2
      .binary "levels/original/lvl2.compress"
levelck1
      .binary "levels/clarkkent/level1.compress"
levelck2
      .binary "levels/clarkkent/level2.compress"
levelck3
      .binary "levels/clarkkent/level3.compress"
levelck4
      .binary "levels/clarkkent/level4.compress"
levelck5
      .binary "levels/clarkkent/level5.compress"
levelck6
      .binary "levels/clarkkent/level6.compress"
levelck7
      .binary "levels/clarkkent/level7.compress"
levelck8
      .binary "levels/clarkkent/level8.compress"
levelck9
      .binary "levels/clarkkent/level9.compress"
levelck10
      .binary "levels/clarkkent/level10.compress"
levelck11
      .binary "levels/clarkkent/level11.compress"
