;;; Format description
;;; ------------------
;;; (byte: wall count, byte: empty count)*$00$00
;;; 2 bytes: offset from screen start, start position (head)
;;; 2 bytes: offset from screen start, exit position (top)
;;; (2 bytes: offset from screen start, snake position)*$xx$ff
;;; (2 bytes: offset from screen start, money bag position)*$xx$ff
;;; 1 byte: map color
;;; 1 byte: bomb count

;;; wall or empty counts > 255 can be encoded as $ff$00$(count-$ff)

;;; It's the responsibility of the writer to build valid data: no
;;; head/ladder-top in last row, no underflow/overflow of walls and
;;; empties, reasonable addresses for all the other items, and enough
;;; stop markers to terminate eventually

scrpage = $fb
leveldata = $fc
scrpos = $fe
moneyCount = $b4
ladderLocation = $b5

incAddr .macro
	inc \1
	bne +
	inc \1 + 1
+
	.endmacro

;;; decompressLevel
;;; ---------------
;;; arguments:
;;;  scrpos: start address of screen to write to (page aligned)
;;;  leveldata: start address of level data

decompressLevel .proc
	clc
	lda #40 ; skip first row
	sta scrpos
	lda scrpos+1
	sta scrpage
	ldy #0

;;; write walls or empty
	ldx #'{wall}'
_writeStride
	lda (leveldata), y
	bne +
	;; first zero -> check if following is also 0
	#incAddr leveldata
	lda (leveldata), y
	bne _nextStride
	;; two zeroes in a row -> exit stride processing
	jmp _writeStrideDone

+
	;; main loop: write stride
	tay
	txa
	ldx #0
-
	sta (scrpos, x)
	#incAddr scrpos
	dey
	bne -

	;; move to next item
	#incAddr leveldata

	tax
_nextStride
	txa
	;; flip wall/empty
	eor #('{wall}' ^ '{empty}')
	tax
	jmp _writeStride
_writeStrideDone
	#incAddr leveldata

;;; write start
	jsr parseOffset

	lda #'{startTop}'
	sta (scrpos), y
	ldy #40
	lda #'{startBtm}'
	sta (scrpos), y

;;; write exit
	jsr parseOffset

	lda scrpos
	sta ladderLocation
	lda scrpos+1
	sta ladderLocation+1

	lda #'{ladderTop}'
	sta (scrpos), y
	ldy #40
	lda #'{ladderBtm}'
	sta (scrpos), y
	; ldy #0 // parseOffset below ensures this

;;; write snakes
	ldx #'{snake}'
-
	jsr parseOffset
	bcs +
	txa
	sta (scrpos), y
	jmp -
+

;;; write money bags
	lda #0
	sta moneyCount
	ldx #'{money}'
-
	jsr parseOffset
	bcs +
	txa
	inc moneyCount
	sta (scrpos), y
	jmp -
+

;;; draw border (left, bottom)
	lda scrpage
	sta scrpos+1
	lda #40
	sta scrpos
	ldy #0

	; left
	ldx #23
-
	lda #$4d
	sta (scrpos), y
	clc
	lda scrpos
	adc #40
	sta scrpos
	bcc +
	inc scrpos+1
+
	dex
	bne -

	; bottom
	lda scrpage
	adc #>(23*40+1)
	sta scrpos + 1
	lda #<(23*40+1)
	sta scrpos

	ldx #39
-
	lda #$51
	sta (scrpos), y
	#incAddr scrpos
	dex
	bne -

	; This adds a pattern after the screen space.
	; It's not strictly necessary but simplifies
	; comparisons.
	; ff ff 00 00 00 00 ff ff ... from 1000 to 1024
	clc
	lda scrpage
	adc #>1000
	sta scrpos + 1
	lda #<1000
	sta scrpos

	ldx #2
-
	txa
	lsr a
	lsr a
	clc
	and #1
	adc #$ff
	sta (scrpos), y
	#incAddr scrpos
	inx
	cpx #26
	bne -

;;; write map color and bomb count
	clc
	lda scrpage
	adc #>$3ee
	sta scrpos + 1
	lda #<$3ee
	sta scrpos
	lda (leveldata), y
	sta (scrpos), y
	iny
	lda (leveldata), y
	sta (scrpos), y
	; WARNING: leveldata wasn't incremented!

	rts
.endproc

;;; compressLevel
;;; ---------------
;;; arguments:
;;;  scrpos: start address of screen to compress (page aligned)
;;;  leveldata: start address for compressed level data

compressLevel .proc
	clc
	lda #40 ; skip first row
	sta scrpos
	lda scrpos+1
	adc #4
	sta scrpage
	ldy #0
	ldx #0

_nextpass
-
	lda scrpos+1
	cmp scrpage
	beq _endpass
	lda (scrpos), y
	#incAddr scrpos
	cmp #'{empty}'		; test for not-empty
	clc
	beq +
	inx
	bne -
	;; ended up with 256 at a time, so store 255, store a 0 of the
	;; other type, then continue counting at 1.
	lda #$ff
	sta (leveldata), y
	#incAddr leveldata
	lda #0
	sta (leveldata), y
	#incAddr leveldata
	inx
	jmp -
+
	txa
	sta (leveldata), y
	#incAddr leveldata

	ldx #1
-
	lda scrpos+1
	cmp scrpage
	beq _endpass
	lda (scrpos), y
	#incAddr scrpos
	cmp #'{wall}' 		; test for not-wall
	clc
	beq +
	inx
	bne -
	;; ended up with 256 at a time, so store 255, store a 0 of the
	;; other type, then continue counting at 1.
	lda #$ff
	sta (leveldata), y
	#incAddr leveldata
	lda #0
	sta (leveldata), y
	#incAddr leveldata
	inx
	jmp -
+
	txa
	sta (leveldata), y
	#incAddr leveldata
	ldx #1
	jmp _nextpass

_endpass
	;; ensure there are exactly two trailing zeroes
	lda #$ff
	ldx #0
	ldy #0

	inx
	dec leveldata
	cmp leveldata
	bne +
	dec leveldata+1
+

	cmp (leveldata), y
	bne _addTwoZeroes

	inx
	dec leveldata
	cmp leveldata
	bne +
	dec leveldata+1
+
	cmp (leveldata), y
	beq _stridesDone
	bne _addZero

_addTwoZeroes
-
	cpx #0
	beq +
	#incAddr leveldata
	dex
+
	tya
	sta (leveldata, x)
	#incAddr leveldata

_addZero
-
	cpx #0
	beq +
	#incAddr leveldata
	dex
+
	tya
	sta (leveldata, x)
	#incAddr leveldata

_stridesDone
;;; start position
	ldx #'{startTop}'
	jsr initScan
	jsr scanSingle
	lda scrpage
	cmp scrpos+1
	bne +
	jmp fail
;;; end position
+
	ldx #'{ladderTop}'
	jsr initScan
	jsr scanSingle
	lda scrpage
	cmp scrpos+1
	bne +
	jmp fail
;;; list of snakes
+
	ldx #'{snake}'
	jsr scanScreen
	lda #$ff
	sta (leveldata), y
	#incAddr leveldata
	sta (leveldata), y
	#incAddr leveldata
;;; list of money bags
	ldx #'{money}'
	jsr scanScreen
	lda #$ff
	sta (leveldata), y
	#incAddr leveldata
	sta (leveldata), y
	#incAddr leveldata

;;; map color and bomb count
	lda scrpage
	sbc #1 ; scrpage was +4 and we need the 3 in $3ee
	sta scrpos+1
	lda #<$3ee
	sta scrpos
	lda (scrpos), y
	sta (leveldata), y
	iny
	lda (scrpos), y
	sta (leveldata), y
	#incAddr leveldata
	#incAddr leveldata

	rts
.endproc

fail .proc
;;; TODO: proper failure reporting
	brk
.endproc

;;; calculates the address (scrpage+*(leveldata+1))*256+*(leveldata)
;;; and stores it in scrpos
parseOffset .proc
	clc
	ldy #0
	lda (leveldata), y
	sta scrpos
	#incAddr leveldata

	lda scrpage
	adc (leveldata), y
	sta scrpos+1
	#incAddr leveldata
	rts
.endproc

;;; scans screen (4 pages before scrpage*256) for symbol in x,
;;; writing the offsets to leveldata
scanScreen .proc
	jsr initScan
-
	jsr scanSingle
	lda scrpage
	cmp scrpos+1
	bne -
	rts
.endproc

initScan .proc
	lda scrpage
	sbc #4
	sta scrpos+1
	lda #40
	sta scrpos
	rts
.endproc

;;; scans from current scrpos to next symbol in x, writing the
;;; offset to leveldata
scanSingle .proc
	ldy #0
	clc
-
	txa
	cmp (scrpos), y
	beq +
	#incAddr scrpos
	lda scrpage
	cmp scrpos+1
	bne -
	rts
+
	lda scrpos
	sta (leveldata), y
	#incAddr leveldata
	lda scrpos+1
	sbc scrpage
	adc #4
	sta (leveldata), y
	#incAddr leveldata
	#incAddr scrpos
	rts
.endproc
